﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    private static UIController _instance;

    public static UIController Instance
    {
        get { return _instance; }
    }

    // Use this for initialization
    bool isPaused;
    public SoundController _soundController;
    public Text scoreTextMesh, finalScoreTextMesh, BestScoreTextMesh;
    int inGameScore = 0;
    public GameObject newScoreObj, ScoreBoard, MainMenuContainer, tiltTutorial, Tutorial, QuitButton, Setting, hudmenu, leaderboard, watchRewardedAd;

    public static bool isRestartPressed = false;
    bool readyToPlay = false;
    public static bool isSound;
    public Button SoundOff;
    int deadCount;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
    }

    void OnEnable()
    {


        deadCount = 0;
        SoundOff.gameObject.SetActive(false);
        playerController.IncreaseScore += OnScoreIncrease;
        playerController.PlayerDead += OnPlayerDead;

        MainMenuContainer.SetActive(true);
        newScoreObj.SetActive(false);
        ScoreBoard.SetActive(false);
        hudmenu.SetActive(false);
        Setting.SetActive(false);
        //leaderboard.SetActive (false);
        //on game end scoreboard menu ,if user presses on restart ,
        //we need to disable the mainmenu and changing player to readyto play.
        if (isRestartPressed)
        {
            MainMenuContainer.SetActive(false);
            isRestartPressed = false;
            readyToPlay = true;
            hudmenu.SetActive(true);
            playerController.currentState = playerController.playerStates.alive;

        }
        else
        {
            playerController.currentState = playerController.playerStates.idle;

        }

        scoreTextMesh.text = "" + 0;

#if UNITY_IOS
		//apple won't like quit button
		Destroy(QuitButton);
#endif
        //to display messsage to developers

#if UNITY_EDITOR

        //assetStoreReviewText.SetActive (true);
#else
		//assetStoreReviewText.SetActive(false);
#endif

        if (PlayerPrefs.GetInt("TempScore") > 0)
        {
            inGameScore = PlayerPrefs.GetInt("TempScore");
            scoreTextMesh.text = "" + inGameScore;
        }


    }

    // Update is called once per frame
    RaycastHit hit;

    void Update()
    {

        //to handle escape key
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (playerController.currentState == playerController.playerStates.idle)
            {

                //Application.Quit ();
            }
            else
            {

                restart();
            }

        }
    }

    public void OnButtonClicks(string incomingName)
    {
        if (hit.collider != null)
            print(hit.distance);

        if (Physics.Raycast(transform.position, transform.forward, out hit, 2.0F))
        {

            if (hit.transform.tag == "ColliderCheck")
            {
                print("Check");
            }
        }

        Debug.Log("clicked on " + incomingName);
        SoundController.Static.PlayClickSound();
        // print(inGameScore);
        switch (incomingName)
        {
            case "Play":
                MainMenuContainer.SetActive(false);

                if (SceneManager.GetActiveScene().name == "Hard")
                    tiltTutorial.SetActive(true);
                else
                    Tutorial.SetActive(true);

                readyToPlay = true;
                playerController.currentState = playerController.playerStates.alive;

                hudmenu.SetActive(true);
                Invoke("lateDeactivateTutorial", 2);
                break;
            case "Home":

                if (isPaused)
                {
                    if (Time.timeScale == 1.0f)
                    {
                        Time.timeScale = 0.0f;
                    }
                    else
                    {

                        Time.timeScale = 1.0f;
                    }
                }
                SceneManager.LoadScene("Welcome");
                //restart ();
                break;

            case "Restart":

                if (isPaused)
                {
                    if (Time.timeScale == 1.0f)
                    {
                        Time.timeScale = 0.0f;

                    }
                    else
                    {

                        Time.timeScale = 1.0f;
                    }
                }
                isRestartPressed = true;

                hudmenu.SetActive(true);
                restart();

                break;

            case "Facebook":
                //FBLoginManager.Instance.ShareScore(inGameScore);
                break;

            case "Setting":
                Debug.Log("Setting");
                playerController.currentState = playerController.playerStates.idle;
                if (!isSound)
                    SoundOff.gameObject.SetActive(false);
                Setting.SetActive(true);
                if (ScoreBoard.activeSelf)
                    ScoreBoard.SetActive(false);

                break;

            case "Back":

                if (isPaused)
                {

                    Time.timeScale = 1.0f;
                    playerController.currentState = playerController.playerStates.alive;
                }
                else
                {

                    //Time.timeScale = 0.0f;
                    playerController.currentState = playerController.playerStates.idle;

                }
                if (Setting.activeSelf)
                {
                    Setting.SetActive(false);
                    //ScoreBoard.SetActive (true);

                }
                else if (ScoreBoard.activeSelf)
                    ScoreBoard.SetActive(true);


                //			if(ScoreBoard.activeSelf)
                //				playerController.currentState = playerController.playerStates.idle;
                //			else
                //				playerController.currentState = playerController.playerStates.alive;
                //
                //			PauseGame();
                break;

            case "SoundOn":

                if (isSound)
                {
                    isSound = false;

                }
                else
                {

                    isSound = true;
                }

                if (!isSound)
                    SoundOff.gameObject.SetActive(false);
                else
                    SoundOff.gameObject.SetActive(true);

                break;


            case "Pause":

                PauseGame();

                break;


            case "Review":
#if UNITY_ANDROID
                Application.OpenURL("market://details?id=com.BloodBrothers.Kukku");
#endif
#if UNITY_IOS
			            Application.OpenURL ("http://play.google.com/store/apps/details?id=com.BloodBrothers.Kukku");
#endif
#if UNITY_WEBPLAYER
			            Application.OpenURL ("http://play.google.com/store/apps/details?id=com.BloodBrothers.Kukku");
#endif
#if UNITY_EDITOR
                Application.OpenURL("http://play.google.com/store/apps/details?id=com.BloodBrothers.Kukku");
#endif

                break;

            case "Quit":

                if (SceneManager.GetActiveScene().name == "GamePlay")
                    SceneManager.LoadScene("Welcome");
                else if (SceneManager.GetActiveScene().name == "Hard")
                    SceneManager.LoadScene("Welcome");
                else if (SceneManager.GetActiveScene().name == "Tough")
                    SceneManager.LoadScene("Welcome");

#if UNITY_ANDROID
                else if (SceneManager.GetActiveScene().name == "Welcome")
                    Application.Quit();
#endif

                break;

            case "Leaderboard":
                leaderboard.SetActive(true);
                GetLoginStatus();
                break;
        }
    }

    public void GetLoginStatus()
    {
        //if (!FB.IsLoggedIn)
        //    FBLoginManager.Instance.Login(true);
        //else
        //    FindObjectOfType<Leaderboards>().GetLeaderboard();
    }


    public void PauseGame()
    {
        if (Time.timeScale == 1.0f)
        {
            playerController.currentState = playerController.playerStates.idle;
            Time.timeScale = 0.0f;
            isPaused = true;
            Setting.SetActive(true);
        }
        else
        {
            playerController.currentState = playerController.playerStates.alive;
            Time.timeScale = 1.0f;
            isPaused = false;
            Setting.SetActive(false);
        }
    }
    public void lateDeactivateTutorial()
    {
        if (SceneManager.GetActiveScene().name == "Hard")
            tiltTutorial.SetActive(false);
        else
            Tutorial.SetActive(false);
    }

    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //Application.LoadLevel (Application.loadedLevelName);
    }

    void OnScoreIncrease(System.Object obj, EventArgs args)
    {
        inGameScore++;
        scoreTextMesh.text = "" + inGameScore;

        SoundController.Static.PlayScoreIncrease();
    }

    void OnPlayerDead(System.Object obj, EventArgs args)
    {
        print("iam dead");
        deadCount++;
        finalScoreTextMesh.text = "Score: " + inGameScore.ToString();
        if (!PlayerPrefs.HasKey("BestScore"))
        {
            PlayerPrefs.SetInt("BestScore", 0);
        }
        if (inGameScore > PlayerPrefs.GetInt("BestScore"))
        {
            PlayerPrefs.SetInt("BestScore", inGameScore);
            BestScoreTextMesh.text = "Best: " +  inGameScore.ToString();
            newScoreObj.SetActive(true);
        }
        else
            BestScoreTextMesh.text = "Best: " + PlayerPrefs.GetInt("BestScore");

        OpenWatchRewardedPanel();
        //ScoreBoard.SetActive (true);

    }

    void OpenWatchRewardedPanel()
    {
        if (UnityAdsManager.Instance)
        {
            if (UnityAdsManager.Instance.isRewardedAdReady())
            {
                watchRewardedAd.SetActive(true);
            }
            else
                ScoreBoard.SetActive(true);
        }

    }

    public void ShowRewardedAd()
    {
        //watchRewardedAd.SetActive(false);
        UnityAdsManager.Instance.ShowRewaredVideoAd();
    }

    public void ShowScoreBoard()
    {
        PlayerPrefs.DeleteKey("TempScore");
        watchRewardedAd.SetActive(false);
        ScoreBoard.SetActive(true);
    }


    public void Reset()
    {
        //FindObjectOfType<RandomBirds>().ChangeAliveBird();
        ////playerController.currentState = playerController.playerStates.idle;
        //isRestartPressed = true;

        //hudmenu.SetActive(true);
        //watchRewardedAd.SetActive(false);
        //deadCount = 0;
        //SoundOff.gameObject.SetActive(false);

        //MainMenuContainer.SetActive(true);
        //newScoreObj.SetActive(false);
        //ScoreBoard.SetActive(false);
        //hudmenu.SetActive(false);
        //Setting.SetActive(false);

        //if (isRestartPressed)
        //{
        //    MainMenuContainer.SetActive(false);
        //    isRestartPressed = false;
        //    readyToPlay = true;
        //    hudmenu.SetActive(true);
        //    playerController.currentState = playerController.playerStates.alive;

        //}
        //else
        //{
        //    playerController.currentState = playerController.playerStates.idle;

        //}

        //OnButtonClicks("Play");
        //scoreTextMesh.text = "" + 0;

        PlayerPrefs.SetInt("TempScore", inGameScore);
        restart();



    }

    public void PostScore(int score)
    {

        //leaderboard.SetActive (true);
        //if (SceneManager.GetActiveScene ().name == "GamePlay") {
        //	new GameSparks.Api.Requests.LogEventRequest_PostClassicScore ()
        //	.Set_ClassicScore (score)
        //	.Send ((GameSparks.Api.Responses.LogEventResponse response) => {

        //		if (response.HasErrors){
        //			Debug.Log ("Erors: " + response.Errors);

        //		}
        //		else
        //		{
        //			Debug.Log ("Successfull");

        //		}
        //});

        //			GameSparks.Api.Messages.NewHighScoreMessage.Listener = (message) =>
        //			{
        //				var leaderboardData = message.LeaderboardData; 
        //				string leaderboardName = message.LeaderboardName; 
        //				string leaderboardShortCode = message.LeaderboardShortCode; 
        //				string messageId = message.MessageId; 
        //				bool? notification = message.Notification; 
        //				var rankDetails = message.RankDetails; 
        //				string subTitle = message.SubTitle; 
        //				string summary = message.Summary; 
        //				string title = message.Title; 
        //				//NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(0), title, summary, Color.blue ,NotificationIcon.Message);
        //				Debug.Log(leaderboardShortCode + leaderboardData.UserName + rankDetails.GlobalCount + subTitle + title + summary);
        //			};
        //
        //			GameSparks.Api.Messages.GlobalRankChangedMessage.Listener = (message) => {
        //				long? gameId = message.GameId; 
        //				string leaderboardName = message.LeaderboardName; 
        //				string leaderboardShortCode = message.LeaderboardShortCode; 
        //				string messageId = message.MessageId; 
        //				bool? notification = message.Notification; 
        //				//GSEnumerable<GSData> scriptData = message.ScriptData; 
        //				string subTitle = message.SubTitle; 
        //				string summary = message.Summary; 
        //				var them = message.Them; 
        //				string title = message.Title; 
        //				var you = message.You; 
        //				NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(0), title, summary, Color.blue ,NotificationIcon.Message);
        //				Debug.Log("GlobalRankChangedMessage: " + title);
        //			};
        //}
        //else if(SceneManager.GetActiveScene ().name == "Hard")
        //{
        //	new GameSparks.Api.Requests.LogEventRequest_PostHardScore ()
        //		.Set_HardScore (score)
        //		.Send ((GameSparks.Api.Responses.LogEventResponse response) => {

        //			if (response.HasErrors)
        //				Debug.Log ("Erors: " + response.Errors);
        //			else
        //				Debug.Log ("Successfull");
        //		});
        //}

        //else if(SceneManager.GetActiveScene ().name == "Tough")
        //{
        //	new GameSparks.Api.Requests.LogEventRequest_PostToughScore ()
        //		.Set_ToughScore (score)
        //		.Send ((GameSparks.Api.Responses.LogEventResponse response) => {

        //			if (response.HasErrors)
        //				Debug.Log ("Erors: " + response.Errors);
        //			else
        //				Debug.Log ("Successfull");
        //		});
        //}
    }


    void OnDisable()
    {
        playerController.IncreaseScore -= OnScoreIncrease;
        playerController.PlayerDead -= OnPlayerDead;
    }
}
