﻿using UnityEngine;
using System.Collections;

public class Accelerometer : MonoBehaviour {

	Transform playerTransform;
	// Use this for initialization
	void Start () {
		//player = FindObjectOfType<playerController> ();
		playerTransform= GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		//transform.Translate (Input.acceleration.x, 0,0);

		if (playerController.currentState == playerController.playerStates.alive) {
			if(playerTransform.position.x >= -2.67f && playerTransform.position.x <= 2.67f)
				AccelerometerMove ();
		}


	}

	void AccelerometerMove()
	{
		Vector3 dir = Vector3.zero;
		dir.x = Input.acceleration.x;
		if (dir.x > Camera.main.orthographicSize)
			dir.x = Camera.main.orthographicSize;
		//dir.z = Input.acceleration.x;
		if (dir.sqrMagnitude > 1)
			dir.Normalize();

		dir *= Time.deltaTime;
		transform.Translate(dir * 10.0f);
	}
}
