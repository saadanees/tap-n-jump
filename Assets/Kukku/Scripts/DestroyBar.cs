﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBar : MonoBehaviour
{
    void Update()
    {
        var playerTrans = GameObject.FindGameObjectWithTag("Player").transform;
        float distance = Vector3.Distance(playerTrans.position, transform.position);
        
        
        if (distance > 50)
        {
            RemoveBar();
        }

    }

    public void RemoveBar()
    {
        Destroy(transform.gameObject);
    }

    
}
