﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsManager : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener
{
    [SerializeField] string _androidGameId = "3716597";
    [SerializeField] string _iOSGameId = "3716596";
    [SerializeField] bool _testMode = true;
    private string _gameId;

    string _rewardedVideoAd = "rewardedVideo";
    string videoAd = "video";
    string bannerAd = "banner";
    
    float videoAdTimeOut = 0f;

    // Start is called before the first frame update

    private static UnityAdsManager _instance;

    public static UnityAdsManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        InitializeAds();
    }

    void Start()
    {
        LoadBanner();
        LoadRewardedAd();
    }

    public void InitializeAds()
    {
        #if UNITY_IOS
            _gameId = _iOSGameId;
        #elif UNITY_ANDROID
        _gameId = _androidGameId;
        #elif UNITY_EDITOR
            _gameId = _androidGameId; //Only for testing the functionality in the Editor
        #endif

        if (!Advertisement.isInitialized && Advertisement.isSupported)
        {
            Advertisement.Initialize(_gameId, _testMode, this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //print(TimeOut());
        
    }
    
    bool TimeOut()
    {
        bool canSkip;
        if (videoAdTimeOut >= 10)
        {
            videoAdTimeOut = 0f;
            canSkip = false;
            
        }
        else
        {
            videoAdTimeOut += Time.deltaTime;
            float seconds = videoAdTimeOut % 60;
            print((int)seconds);

            canSkip = true;
        }

        return canSkip;
    }


    // Implement a method to call when the Load Banner button is clicked:
    public void LoadBanner()
    {
        // Set up options to notify the SDK of load events:
        BannerLoadOptions options = new BannerLoadOptions
        {
            loadCallback = OnBannerLoaded,
            errorCallback = OnBannerError
        };

        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);

        // Load the Ad Unit with banner content:
        Advertisement.Banner.Load(bannerAd, options);
    }

    // Implement code to execute when the loadCallback event triggers:
    void OnBannerLoaded()
    {
        Debug.Log("Banner loaded");
        ShowBannerAd();

    }

    // Implement code to execute when the load errorCallback event triggers:
    void OnBannerError(string message)
    {
        Debug.Log($"Banner Error: {message}");
        // Optionally execute additional code, such as attempting to load another ad.
    }


    void ShowBannerAd()
    {
        Advertisement.Banner.Show(bannerAd);
        Debug.Log("Banner showing");
    }

    void HideBannerAd()
    {
        // Hide the banner:
        Advertisement.Banner.Hide();
    }

    public void LoadRewardedAd()
    {
        if(Advertisement.isInitialized)
        {
            // IMPORTANT! Only load content AFTER initialization (in this example, initialization is handled in a different script).
            Debug.Log("Loading Ad: " + _rewardedVideoAd);
            Advertisement.Load(_rewardedVideoAd, this);
        }
        
    }

    public void ShowRewaredVideoAd()
    {
        Advertisement.Show(_rewardedVideoAd, this);
    }

   

    public IEnumerator ShowVideoAd()
    {

        while (!Advertisement.isInitialized)
        {
            yield return null;
        }

        Advertisement.Show(videoAd);
    }


    public bool isRewardedAdReady()
    {
        //Need to change this
        return Advertisement.isInitialized;
    }

    public void OnInitializationComplete()
    {
        Debug.Log("Unity Ads initialization complete.");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }

    public void OnUnityAdsAdLoaded(string rewardedVideo)
    {
        Debug.Log("Unity Ads Rewarded Ad Loaded");
        
    }


    public void OnUnityAdsShowStart(string rewardedVideo)
    {
        Debug.Log("Unity Ads Rewarded Ad Started");
        HideBannerAd();
    }

    public void OnUnityAdsShowClick(string rewardedVideo)
    {
        Debug.Log("Unity Ads Rewarded Ad Clicked");
        //ShowBannerAd();
    }

    // Implement the Show Listener's OnUnityAdsShowComplete callback method to determine if the user gets a reward:
    public void OnUnityAdsShowComplete(string rewardedVideo, UnityAdsShowCompletionState showCompletionState)
    {
        if (rewardedVideo.Equals(_rewardedVideoAd) && showCompletionState.Equals(UnityAdsShowCompletionState.COMPLETED))
        {
            Debug.Log("Unity Ads Rewarded Ad Completed");
            ShowBannerAd();
            UIController.Instance.Reset();
            // Grant a reward.
        }
    }

    // Implement Load and Show Listener error callbacks:
    public void OnUnityAdsFailedToLoad(string rewardedVideo, UnityAdsLoadError error, string message)
    {
        Debug.Log($"Error loading Ad Unit {rewardedVideo}: {error.ToString()} - {message}");
        // Use the error details to determine whether to try to load another ad.
    }

    public void OnUnityAdsShowFailure(string rewardedVideo, UnityAdsShowError error, string message)
    {
        Debug.Log($"Error showing Ad Unit {rewardedVideo}: {error.ToString()} - {message}");
        // Use the error details to determine whether to try to load another ad.
    }


}
