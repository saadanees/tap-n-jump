﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrounderMover : MonoBehaviour
{
   // Transform currentPos;
    public GameObject playerGO;
    // Start is called before the first frame update
    void Start()
    {
       // currentPos = GameObject.FindGameObjectWithTag("Player").transform;
        print(playerGO.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
       float distance = Vector3.Distance(playerGO.transform.position, transform.position);

       
        Vector3 offset = new Vector3(playerGO.transform.position.x, playerGO.transform.position.y + 60, playerGO.transform.position.z);
        if (distance > 20)
        {
            transform.Translate(offset * Time.deltaTime);
            //print("distance: " + distance);
        }
            
    }
}
