﻿using UnityEngine;
using System.Collections;

public class RandomBirds : MonoBehaviour {

	public Sprite[] birdsAliveArray;
	public Sprite[] birdsDeadArray;
	public GameObject birdObj;
	Transform thisTrans;

	public bool blueDead, pinkDead, greenDead, deadOn;
	// Use this for initialization
	void Start () {
		deadOn = false;
        ChangeAliveBird();

    }
	
    public void ChangeAliveBird()
    {
        (GetComponent<Renderer>() as SpriteRenderer).sprite = birdsAliveArray[Random.Range(0, birdsAliveArray.Length)];
        deadOn = false;
        transform.rotation = new Quaternion(0,0,0,0);
    }

	// Update is called once per frame
	void Update () {
		if (deadOn)
        {
			transform.Rotate (Vector3.forward * 80);
            Debug.Log("rotate" + deadOn);
            deadOn = false;
			
	    }
        //if(!deadOn)
        //{
        //    transform.Rotate(Vector3.zero);
        //    Debug.Log("reset " + deadOn + " " + transform.rotation);
        //}
	}

	//call dead bird
	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.tag== "Enemy")
		{
			if((GetComponent<Renderer>() as SpriteRenderer).sprite == birdsAliveArray[0])
			{
				(GetComponent<Renderer>() as SpriteRenderer).sprite = birdsDeadArray[0];
				deadOn = true;
				//iTween.ShakePosition(birdObj, iTween.Hash("y", 0.3f, "time", 0.8f, "delay", 8.0f));
				//transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
				Debug.Log("Blue_Dead");
			}
			else if ((GetComponent<Renderer>() as SpriteRenderer).sprite == birdsAliveArray[1])
			{
				(GetComponent<Renderer>() as SpriteRenderer).sprite = birdsDeadArray[1];
				deadOn = true;
				//transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
				Debug.Log("Green_Dead");
			}
			else if ((GetComponent<Renderer> () as SpriteRenderer).sprite == birdsAliveArray [2]) {
				(GetComponent<Renderer> () as SpriteRenderer).sprite = birdsDeadArray [2];
				deadOn = true;
				//transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
				Debug.Log ("Pink_Dead");
			} else
				return;	
		}
	}
}
