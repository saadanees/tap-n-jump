﻿using UnityEngine;
using System.Collections;

public class CloseButton : MonoBehaviour {

	public GameObject Obj;

	public void CloseThis()
	{
		if(Obj.activeSelf)
			Obj.SetActive (false);
	}
}
