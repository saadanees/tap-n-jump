﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrivacyPolicy : MonoBehaviour
{
   public void OpenPrivacyPolicy()
    {
        Application.OpenURL("http://bloodbrothersgames.com/privacy-policy.html");
    }
}
