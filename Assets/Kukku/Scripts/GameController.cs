﻿using UnityEngine;
using System.Collections;
using System;
public class GameController : MonoBehaviour
{

    // Use this for initialization
    //public GameObject parentObj;
	public GameObject barGroup; //bargroup,contains 3 bars left and right
	public float distanceBetweenBarGroups; 
	public Transform playerTransform;
	public Color[] colors;//to be used for changing camera background colors 
	public float lastPlayerPosition;
    bool isBarInitiated;
 
	void Start ()
	{
		
		playerTransform = GameObject.FindGameObjectWithTag ("Player").transform;
		OnGameStart ();

        isBarInitiated = false;

    }
	
	// Update is called once per frame
	void Update ()
	{
	

		//if player player somes 6 units distance from its last distance ,then we will create new bar group
		if (playerTransform != null && playerTransform.position.y - lastPlayerPosition > 6) {

			createNewGroup ();

			lastPlayerPosition = playerTransform.position.y; // to store the present bird position y .
		}
        //if (isBarInitiated)
        //{
        //    print("yes");
        //    if (playerTransform != null && playerTransform.position.y + lastPlayerPosition > lastPlayerPosition + 66f)
        //    {
        //        isBarInitiated = false;
        //        print("OK");
        //        // Remove bar
        //        transform.GetChild(0).GetComponent<DestroyBar>().RemoveBar();
        //    }
        //    else
        //    {
        //        isBarInitiated = false;
        //    }
            
        //}


    }

	int groupIndex = 1;
	void createNewGroup ()
	{

		GameObject barGroupNewInstance = GameObject.Instantiate (barGroup) as GameObject;

		barGroupNewInstance.transform.position = new Vector3 (-3, distanceBetweenBarGroups * groupIndex, 0); //this will create new bars group
        //barGroupNewInstance.AddComponent<DestroyBar>();
        barGroupNewInstance.transform.SetParent(transform);
        groupIndex++;
        isBarInitiated = true;

       
    }

	void OnGameStart ()
	{
		//to change background color RANDOMLY
		Camera.main.backgroundColor = colors [UnityEngine.Random.Range (0, colors.Length - 1)];
	}
}
