﻿using UnityEngine;
using System.Collections;

public class PlayAnim : MonoBehaviour {

	public GameObject BG, loading, logo, panel;
	public Color[] colors;
	// Use this for initialization
	void Start () {
		Camera.main.backgroundColor = colors [UnityEngine.Random.Range (0, colors.Length - 1)];
		panel.SetActive (false);
		Invoke ("HideCanvas", 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.Escape)) {
			if (playerController.currentState == playerController.playerStates.idle) {

				Application.Quit ();
			} 
		}
	}

	public void LoadFacebook()
	{
		Application.OpenURL("fb://page/783435941697095");
//		Application.OpenURL("https://www.facebook.com/783435941697095");
	}

	public void LoadTwitter()
	{
		Application.OpenURL("twitter://user?screen_name=bloodbrosgames ");
	}
	void HideCanvas()
	{
		BG.SetActive (false);
		loading.SetActive (false);
		logo.SetActive (false);
		panel.SetActive (true);
	}

	public void PlayLevel(string SceneToLoad)
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (SceneToLoad);
	}


}
